/*!  Classe Gestionnaire - Instance de gestion des outils de la simulation*/
/**
* \file Gestionnaire.hpp
* \author Justin Goutey & Maxime Sibellas
* \date 8 février 2016
* \brief Descriptif de la classe Gestionnaire
* \addtogroup Simulation
*/

#ifndef GESTIONNAIRE_HPP
#define GESTIONNAIRE_HPP

#include <vector>
#include <string>
#include "Feu.h"
#include "Glace.h"


/*! \class Gestionnaire
    \brief Classe contenant les outils de gestion de la simulation
*/

class Gestionnaire
{
protected:

    static int                          compteur_;   //!< Compteur d'entité de gestionnaire
    int                                 nbFeu_;      //!< Nombre d'individu de type feu pour la simulation
    int                                 nbGlace_;    //!< Nombre d'individu de type glace pour la simulation
    std::vector<Feu *>                  feu_;        //!< Liste des individus de type Feu
    std::vector<Glace *>                glace_;      //!< Liste des individus de type Glace
    std::vector<std::vector<Individu*>> carte_;      //!< Map à l'instant t
    std::vector<std::vector<Individu*>> nextMap_;    //!< Map à l'instant t+1
    std::vector<int>                    naissancesG_;//!< Ressencement des naissances d'individu Glace
    std::vector<int>                    mortsG_;     //!< Ressencement des morts d'individu Glace
    std::vector<int>                    naissancesF_;//!< Ressencement des naissances d'individu Feu
    std::vector<int>                    mortsF_;     //!< Ressencement des morts d'individu Feu

public:
    //! Constructeur
    /*!
     * \sa Gestionnaire(int,int), Gestionnaire(int,int,int,int)
     */
    Gestionnaire();

    //! Constructeur de test
    /*!
     * N'invoque que deux éléments. Paramètre la taille de la map.
     * \param x Abscisse initiale
     * \param y Ordonnée initiale
     * \sa Gestionnaire(), Gestionnaire(int,int,int,int)
     */
    Gestionnaire(int,int);

    //! Constructeur spécialisé
    /*!
     * \param int nbGlace Nombre d'individu de Glace
     * \param int nbFeu Nombre d'individu de Feu
     * \param int abscisseMap Valeur de la largeur de la map
     * \param int ordonneeMap Valeur de la hauteur de la map
     */
    Gestionnaire(int, int, int, int);

    //! Getter du compteur
    /*!
     * Renvoi la valeur du compteur d'entité
     * \sa getFeu, getGlace, getCarte
     */
    static int getCompteur();

    //! Getter du parametre feu_;
    /*!
     * \return Liste des individus de type Feu
     * \sa getCompteur, getGlace, getCarte
     */
    std::vector<Feu *> getFeu();

    //! Getter du parametre glace_
    /*!
     * \return Liste des individus de type Glace
     * \sa getCompteur, getFeu, getCarte
     */
    std::vector<Glace *> getGlace();

    //! Getter du parametre carte_
    /*!
     * \return Carte à l'état t
     * \sa getCompteur, getFeu, getGlace
     */
    std::vector<std::vector<Individu*>> getCarte();


    //! Procédure de mise à jour de la simulation
    /*!
     * Met à jour la carte à t (carte_) avec la carte à t+1 (nextMap) et remise à zéro de la carte à t+1.
     * \sa iteration, deroulement
     */
    void miseAJour();

    //! Procédure d'itération de la simulation
    /*!
     * Effectue une itération du système
     * \param iter Numéro de l'itération
     * \sa miseAJour, deroulement
     */
    void iteration(int);

    //! Procédure d'affichage de la map
    /*!
     * Affiche la map à l'instant t. Identifie les individus par la première lettre de leur type.
     * \sa deroulement, getInfo
     */
    void afficher();

    //! Procédure de déroulement de la simulation
    /*!
     * \param iter Nombre d'itérations à dérouler
     * \sa miseAJour, iteration, afficher, getInfo
     */
    void deroulement(int);

    //! Procédure d'affichage des informations du système
    /*!
     * Affiche les informations relatives à chaque individu et à la map
     * \sa afficher, deroulement
     */
    void getInfo();

    //! Procédure de sauvegarde de la matrice de présence résultant d'une simulation
    /*!
     * Sauvegarde la matrice résultat dans un fichier CSV
     * \param fileFeu Path du fichier de sauvegarde de la classe Feu
     * \param fileGlace Path du fichier de sauvegarde de la classe Glace
     *
     * \sa saveDemographie, restitution
     */
    void sauvegarde(std::string, std::string);


    //! Procédure de sauvegarde des résultats démographiques de la simulation
    /*!
     * Sauvegarde le nombre de morts et de naissance selon les iterations ainsi que l'âge des individus en fin de simulation
     * \param fileFeu Path du fichier de sauvegarde de la classe Feu
     * \param fileGlace Path du fichier de sauvegarde de la classe Glace
     * \param iter Nombre d'itération par simulation
     * \sa sauvegarde
     */
    void saveDemographie(std::string, std::string,int);

    //! Procédure de restitution de l'état d'occupation du terrain
    /*!
     * Récupère la matrice correspondante à un fichier .csv.
     * \param file Path du fichier de sauvegarde
     * \return Matrice résultante
     * \sa sauvegarde
     */
    std::vector<std::vector<int>> restitution(std::string);


    //! Procédure de restitution des résultats démographiques de la simulation
    /*!
     * Récupère les données démographiques correspondantes à un fichier .csv de deux lignes : une pour les naissances cumulées, l'autre pour les morts cumulées
     * \param fileFeu path du fichier de sauvegarde pour l'espèce Feu
     * \param fileGlace path du fichier de sauvegarde pour l'espèce Glace
     * \param iter nombre d'itération par simulation
     * \return Matrice résultante
     * \sa sauvegarde
     */
    std::vector<std::vector<int>> restitutionDemo(std::string, std::string, int);

    //! Destructeur
    /*!
     *
     */
    ~Gestionnaire();




};

#endif
