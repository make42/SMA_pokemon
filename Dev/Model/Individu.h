/*!  Classe Individu- Instance mère des agents*/
/**
* \file Individu.hpp
* \author Justin Goutey & Maxime Sibellas
* \date 15 décembre 2015
* \brief Descriptif de la classe Individu
* \addtogroup Simulation
*/


#ifndef INDIVIDU_H
#define INDIVIDU_H

#include <iostream>
#include <cassert>
#include <vector>
#include "../Control/Random.h"


#define MAX_PV 100
#define MAX_ATK 30


/*! \class Individu
    \brief Classe mère des instances d'agents étudiés
*/

class Individu{
protected :
    static int      compteur_;      //!< Permet de compter le nombre d'individus sur la carte
    int             x_;             //!< Abscisse
    int             y_;             //!< Ordonnée
    int             pointsDeVie_;   //!< Nombre de point de vie de l'individu
    int             attaque_;       //!< Points d'attaque de l'individu
    int             age_;           //!< Âge de l'individu
    bool            conflit_;       //!< Traduit l'état de conflit
    Individu *      adversaire_;    //!< Adversaire lors du conflit
    bool            porteOeuf_;     //!< Permet de distinguer les individus porteurs d'un oeuf
    int             tempoPonte_;    //!< Parametre d'attente avant nouvel accouplement

public :
    //! Constructeur par défaut
    /*!
     * \sa Individu(int x, int y, int p, int a, int ag), Individu(const Individu&)
     */
    Individu();

    //! Constructeur prenant en paramètre les caractéristiques d'un individu.
    /*!
     *    \param x abscisse de l'individu
     *    \param y ordonnée de l'individu
     *    \param pdv points de vie de l'individu
     *    \param atk valeur d'attaque de l'individu
     *    \param ag âge de l'individu
     *    \sa Individu(), Individu(const Individu&)
    */
    Individu(int,int,int,int,int);

    //! Constructeur par copie
    /*!
     *  Permet la création d'un individu par copie.
     *  \param I Individu à copier
     *  \sa Individu(), Individu(int, int, int, int, int)
     */
    Individu(const Individu&);

    //! Getter de l'abscisse
    /*!
     * \return Abscisse de l'individu
     * \sa getY(), getPointsDeVie(), getAttaque(), getAge(), isReproductionActive(), getMap()
     */
    int getX();

    //! Getter de l'ordonnée
    /*!
     * \return Ordonnée de l'individu
     * \sa getX(), getPointsDeVie(), getAttaque(), getAge(), isReproductionActive(), getMap()
     */
    int getY();

    //! Getter de points de vie
    /*!
     * \return Valeur de points de vie
     * \sa getX(), getY(), getAttaque(), getAge(), isReproductionActive(), getMap()
     */
    int getPointsDeVie();

    //! Getter de l'attribut age_
    /*!
     * \return Valeur de l'age de l'individu
     */
    int getAge();

    //! Setter de l'adversaire de l'entité
    /*!
     * \param i Nouvelle valeur de l'adversaire
     */
    void setAdversaire(Individu *);

    //! Setter de la temporisation de reproduction
    /*!
     * \param t Nouvelle valeur de la temporisation
     */
    void setTempoPonte(int);


    //!Set l'attribut porteOeuf_ à son contraire
    /*!
     * Active ou désactive le port de l'oeuf
     */
    void switchPorteOeuf();

    //!Set l'attribut conflit_ à true
    /*!
     * Active l'état de conflit
     */
    void switchConflictOn();

    //!Set l'attribut conflit_ à false
    /*!
     * Désactive l'état de conflit
     */
    void switchConflictOff();

    //! Procédure de vieillissement de l'individu
    /*!
     * Augmente l'âge de l'individu
     */
    void vieillissement();

    //! Fonction de validation de reproduction d'un individu
    /*!
     * \return Valeur du test de reproduction
     */
    bool isReproductible();


    //! Fonction de validation de la ponte d'un individu
    /*!
     * Methode virtuelle à destination des classes filles
     * \return Valeur du test de ponte
     */
    virtual bool isPondable() = 0;

    //! Fonction de détection d'un ennemi
    /*!
     * \return Paire contenant les coordonnées de l'ennemi le plus proche dans le champ de vision. x et y valent -1 s'il n'y a pas d'ennemi.
     * \sa presenceAllie()
     */
    virtual std::pair<int,int> presenceEnnemi(std::vector<std::vector<Individu*>> ) =0;

    //! Fonction de détection d'un allié
    /*!
     * \return Paire contenant les coordonnées de l'allié le plus proche dans le champ de vision. x et y valent -1 s'il n'y a pas d'allié.
     * \sa presenceEnnemi()
     */
    virtual std::pair<int,int> presenceAllie(std::vector<std::vector<Individu*>>) =0;


    //! Fonction de récupération du type
    /*!
     * Methode virtuelle destinée aux sous classes
     */
    virtual std::string getType()=0;

    //! Fonction de choix de la direction
    /*!
     * \param dirx abscisse du but
     * \param diry ordonnée du but
     * \param sens du déplacement (opposé à si <0 ou vers à si >0)
     * \param map sur laquelle se déplace l'entité
     *
     * \sa deplacement()
     */
    void direction(int,int,int,std::vector<std::vector<Individu*>> );

    //! Fonction de déplacement d'une entité
    /*!
     * \param map Map sur laquelle se déplace l'entité
     * \param nextMap Map sur laquelle est projetée l'entité à t+1
     *
     * \return Nouvelles coordonnées de l'entité
     */
    virtual std::pair<int,int> deplacement(std::vector<std::vector<Individu*>>,std::vector<std::vector<Individu*>>) =0;


    //! Fonction appliquant une iteration à une entité
    /*!
     * Effectue l'action nécessaire à l'iteration d'un individu
     * \param map Map où se ballade l'entité
     * \param nextMap Map sur laquelle est projetée l'entité à t+1
     *
     * \return Nouveaux coordonnées de l'entité
     */
    std::pair<int,int> iteration(std::vector<std::vector<Individu*>>, std::vector<std::vector<Individu*>>);


    //! Fonction de détermination de l'état de santé
    /*!
     * \return Vrai si les points de vie sont supérieurs à 50% de la vie max
     */
    bool estEnPleineForme();

    //! Fonction rendant effectif la perte de point de vie
    /*!
     * \param d dégâts subits
     */
    void subirDegats(int);

    //! Procédure de résolution de conflit avec l'adversaire
    /*!
     *  Soustrait les points de vie.
     *  Gère le cas où l'ennemi meurt en désactivant le conflit et en supprimant le pointeur vers l'adversaire
     */
    void resoudreConflit();

    //! Procédure d'affichage des informations d'un individu
    /*!
     * Affiche les informations vitales de l'entité
     */
    void getInfo();


    //! Destructeur de l'entité
    /*!
     *  Destructeur virtuel pour les classes filles
     */
    virtual ~Individu()=0;

};




#endif