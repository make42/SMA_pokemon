

/**
* \file Feu.cpp
* \brief Déclaration des méthodes de la classe d'agent de type Feu
* \author Justin Goutey & Maxime Sibellas
* \date 15 décembre 2015
* \addtogroup Simulation
*/


#include "Feu.h"

int Feu::xNid_=4;
int Feu::yNid_=4;

//---------------------------------
//  Constructeur par défaut
//---------------------------------
Feu::Feu()=default;


//---------------------------------
//  Constructeur spécialisé
//---------------------------------
Feu::Feu(int x, int y, int pdv, int atk, int age) : Individu(x, y, pdv, atk, age){

}


//---------------------------------
//  Constructeur par copie
//---------------------------------
Feu::Feu(const Feu& f) : Individu(f.x_, f.y_, f.pointsDeVie_, f.attaque_, f.age_){

}


//---------------------------------
//  Getter de l'attribut de classe xNid_
//---------------------------------
int Feu::getXNid(){
    return xNid_;
}

//---------------------------------
//  Setter de l'attribut de classe xNid_
//---------------------------------
void Feu::setXNid(int x){
    xNid_= x;
}

//---------------------------------
//  Getter de l'attribut de classe yNid_
//---------------------------------
int Feu::getYNid(){
    return yNid_;
}

//---------------------------------
//  Setter de l'attribut de classe yNid_
//---------------------------------
void Feu::setYNid(int y) {
    yNid_ = y;
}


//---------------------------------
//  Fonction de test pour la création d'un nouvel individu
//---------------------------------
bool Feu::isPondable(){
    return(porteOeuf_ && x_<=Feu::xNid_ && y_<=Feu::yNid_);
}


//---------------------------------
//  Fonction de détection d'un ennemi
//      Renvoi les coordonnées de l'ennemi
//          (-1,-1) si absence d'ennemi
//      Active le conflit si l'ennemi est au contact
//---------------------------------
std::pair<int,int> Feu::presenceEnnemi(std::vector<std::vector<Individu*>> map) {
    std::pair<int,int>  pair;
    int                 x=-1;
    int                 y=-1;

    // Parcours de la map
    for(int i=std::max(x_-2,0);i<std::min(x_+2, (int) map.size() - 1); i++){
        for(int j=std::max(y_-2,0);j<=std::min(y_+2, (int) map[0].size() - 1); j++){
            if(map[i][j] != NULL && map[i][j]->getType() == "Glace"){
                // Si ennemi au contact
                if((i<=x_+1 && i>=x_-1) && (j<=y_+1 && j>=y_-1)){
                    conflit_=true;
                    adversaire_= map[i][j];
                    adversaire_->setAdversaire(this);
                    adversaire_->switchConflictOn();
                }
                // Récupération de la position de l'ennemi
                x=i;
                y=j;
            }
        }
    }
    pair = std::make_pair(x,y);
    return pair;
}


//---------------------------------
//  Fonction de détection d'un allie
//      Renvoi les coordonnées de l'allie
//          (-1,-1) si absence de l'allie
//---------------------------------
std::pair<int,int> Feu::presenceAllie(std::vector<std::vector<Individu*>> map) {
    std::pair<int,int>  pair;
    int                 x=-1;
    int                 y=-1;

    for(int i=std::max(x_-2,0);i<=std::min(x_+2, (int) map.size() - 1); i++){
        for(int j=std::max(y_-2,0);j<=std::min(y_+2, (int) map[0].size() - 1); j++){
            // S'il y a un allié et qu'il est plus proche qu'un précédent allié enregistré
            if(map[i][j] != NULL && map[i][j] != map[x_][y_] && map[i][j]->getType() == "Feu" ){
                // S'il est au contact et qu'il peut y avoir reproduction
                if((i<=x_+1 && i>=x_-1) && (j<=y_+1 && j>=y_-1) && isReproductible() && map[i][j]->isReproductible()){
                    porteOeuf_=true;
                    tempoPonte_ =20;
                    map[i][j]->setTempoPonte(20);
                }
                x=i;
                y=j;
            }
        }
    }

    pair = std::make_pair(x,y);
    return pair;
}


//---------------------------------
//  Fonction de déplacement d'un individu
//      Retourne la nouvelle position de l'individu
//---------------------------------
std::pair<int,int> Feu::deplacement(std::vector<std::vector<Individu*>> map, std::vector<std::vector<Individu*>> nextMap) {
    std::pair<int,int>      coordEnnemi = presenceEnnemi(map),
            coordAllie = presenceAllie(map),
            coordNid = std::make_pair(xNid_,yNid_);

    bool                    bEnemy =(coordEnnemi.first!=-1),
            bAllie =(coordAllie.first!=-1);

    double                  rand = MtRandReal();

    if(conflit_){
        resoudreConflit();
    }
        // Faible OU jeune OU porteur d'oeuf
    else if(!estEnPleineForme() || age_<5 || porteOeuf_){
        if(bAllie && bEnemy){
            if(rand<=0.1){
                direction(-1, -1, 1, nextMap);
            }
            else if(rand <= 0.2){
                direction(coordEnnemi.first, coordEnnemi.second, -1, nextMap);
            }
            else if (rand <= 0.5){
                direction(coordAllie.first, coordAllie.second, 1, nextMap);
            }
            else{
                direction(coordNid.first, coordNid.second, 1, nextMap);
            }
        }
        else if(bAllie && !bEnemy){
            if(rand<=0.25){
                direction(-1, -1, 1,nextMap);
            }
            else if (rand <= 0.6){
                direction(coordAllie.first, coordAllie.second, 1,nextMap);
            }
            else{
                direction(coordNid.first, coordNid.second, 1,nextMap);
            }
        }
        else if (!bAllie && bEnemy){
            if(rand<=0.2){
                direction(-1, -1, 1, nextMap);
            }
            else if(rand <= 0.45){
                direction(coordEnnemi.first, coordEnnemi.second, -1,nextMap);
            }
            else{
                direction(coordNid.first, coordNid.second, 1, nextMap);
            }
        }
        else {
            if(rand<=0.35){
                direction(-1, -1, 1, nextMap);
            }
            else{
                direction(coordNid.first, coordNid.second, 1, nextMap);
            }
        }
    }
        //Non porteur d'oeuf ET periode de reproduction
    else if(!porteOeuf_ ){
        if(bAllie && bEnemy){
            if(rand<=0.2){
                direction(-1, -1, 1, nextMap);
            }
            else if(rand <= 0.5){
                direction(coordEnnemi.first, coordEnnemi.second, -1, nextMap);
            }
            else{
                direction(coordAllie.first, coordAllie.second, 1, nextMap);
            }
        }
        else if(bAllie && !bEnemy){
            if(rand<=0.4){
                direction(-1, -1, 1, nextMap);
            }
            else{
                direction(coordAllie.first, coordAllie.second, 1, nextMap);
            }
        }
        else if (!bAllie && bEnemy){
            if(rand<=0.4){
                direction(-1, -1, 1, nextMap);
            }
            else{
                direction(coordEnnemi.first, coordEnnemi.second, -1, nextMap);
            }
        }
        else {
            direction(-1, -1, 1, nextMap);
        }
    }
        // En pleine forme ET non période de reproduction
    else if(estEnPleineForme() ){
        if(bAllie && bEnemy){
            if(rand<=0.1){
                direction(-1, -1, 1, nextMap);
            }
            else if(rand <= 0.4){
                direction(coordEnnemi.first, coordEnnemi.second, -1, nextMap);
            }
            else{
                direction(coordAllie.first, coordAllie.second, 1, nextMap);
            }
        }
        else if(bAllie && !bEnemy){
            if(rand<=0.4){
                direction(-1, -1, 1,nextMap);
            }
            else{
                direction(coordAllie.first, coordAllie.second, 1, nextMap);
            }
        }
        else if (!bAllie && bEnemy){
            if(rand<=0.25){
                direction(-1, -1, 1, nextMap);
            }
            else{
                direction(coordEnnemi.first, coordEnnemi.second, -1,nextMap);
            }
        }
        else {
            direction(-1, -1, 1, nextMap);
        }
    }
    else{
        direction(-1, -1, 1, nextMap);
    }

    return std::make_pair(x_,y_);
}


//---------------------------------
//  Fonction de récupération du type
//---------------------------------
std::string Feu::getType() {
    return "Feu";
}


Feu::~Feu(){

}

