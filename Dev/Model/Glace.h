
/*!  Classe Glace - Instance d'agent de type Glace*/

/**
* \file Glace.hpp
* \brief Descriptif de la classe d'agent de type Glace
* \author Justin Goutey & Maxime Sibellas
* \date 15 décembre 2015
* \addtogroup Simulation
*/


#ifndef GLACE_H
#define GLACE_H

#include "Individu.h"

/*! \class Glace
    \brief Classe d'instance de type Glace
*/

class Glace: public Individu{
protected :
    static int      xNid_; //!< Abscisse de la position du nid
    static int      yNid_; //!< Ordonnée de la position du nid

public :
    //! Constructeur par défaut
    /*!
     * \sa Glace(int,int,int,int,int), Glace(const Glace &)
     */
    Glace();

    //! Constructeur avec paramètres
    /*!
     * Permet la création d'un individu personnalisé.
         \param x abscisse de l'individu
         \param y ordonnée de l'individu
         \param pdv points de vie de l'individu
         \param atk valeur d'attaque de l'individu
         \param age âge de l'individu
         \sa Glace(), Glace(const Glace &)
     */
    Glace(int,int,int,int,int);

    //! Constructeur par copie
    /*!
     * Permet le dédoublement d'un individu
     * \param G agent de type glace
     * \sa Glace(), Glace(int,int,int,int,int)
     */
    Glace(const Glace&);

    //! Getter de l'attribut de classe de l'abscisse du nid
    /*!
     * \return Abscisse du nid
     * \sa  getYNID, getType
     */
    static int  getXNid();

    //! Setter de l'attribut de classe de l'abscisse du nid
    /*!
     * \param Nouvelle abscisse du nid
     * \sa setYNid
     */
    static void setXNid(int);

    //! Getter de l'attribut de classe de l'ordonnée du nid
    /*!
     *  \return Ordonnée du nid
     *  \sa getXNid, getType
     */
    static int  getYNid();

    //! Setter de l'attribut de classe de l'ordonnée du nid
    /*!
     *  \param y Nouvelle ordonnée du nid
     *  \sa setXNid
     */
    static void setYNid(int);

    //! Fonction de test pour la création d'un nouvel individu
    /*!
     * \return Résultat du test
     */
    bool isPondable();

    //! Procédure de détection d'ennemi
    /*!
     *  \param map Map sur laquelle se déplace l'entité
     *
     *  \return Nouvelles coordonnées de l'entité
     *  \sa presenceAllie, deplacement
     */
    std::pair<int,int> presenceEnnemi(std::vector<std::vector<Individu*>>);

    //! Procédure de détection d'allie
    /*!
     *  \param map Map sur laquelle se déplace l'entité
     *
     *  \return Nouvelles coordonnées de l'entité
     *  \sa presenceEnnemin deplacement
     */
    std::pair<int,int> presenceAllie(std::vector<std::vector<Individu*>>);


    //! Fonction de déplacement d'une entité
    /*!
     * \param map Map sur laquelle se déplace l'entité
     * \param nextMap Map sur laquelle est projetée l'entité à t+1
     *
     * \return Nouvelles coordonnées de l'entité
     * \sa presenceAllie, presenceEnnemi
     */
    std::pair<int,int> deplacement(std::vector<std::vector<Individu*>>,std::vector<std::vector<Individu*>>);

    //! Procédure de récupération du type
    /*!
     * \return Valeur du type
     * \sa getXNid, getYNid
     */
    std::string getType();

    //! Destructeur
    /*!
     *
     */
    ~Glace();

};

#endif
