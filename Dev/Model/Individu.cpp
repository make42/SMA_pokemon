
/**
* \file Individu.cpp
* \brief Déclaration des méthodes de la classe Individu
* \author Justin Goutey & Maxime Sibellas
* \date 15 décembre 2015
* \addtogroup Simulation
*
*/

#include "Individu.h"

int Individu::compteur_ =0;

//---------------------------------
//  Constructeur par défaut
//---------------------------------
Individu::Individu(){
	compteur_++;
	x_=0;
	y_=0;
	pointsDeVie_=MAX_PV;
	attaque_=0;
	age_=0;
	conflit_ = false;
	porteOeuf_=false;
}


//---------------------------------
//  Contructeur spécialisé
//---------------------------------
Individu::Individu(int x, int y, int pdv, int atk, int age): pointsDeVie_(pdv), attaque_(atk), age_(age), porteOeuf_(false){
    x_=x;
    y_=y;
    compteur_++;
    conflit_ = false;
    tempoPonte_=0;
}


//---------------------------------
//  Constructeur par copie
//---------------------------------
Individu::Individu(const Individu& i): x_(i.x_), y_(i.y_), pointsDeVie_(i.pointsDeVie_), attaque_(i.attaque_), age_(i.age_), conflit_(i.conflit_), porteOeuf_(false), tempoPonte_(i.tempoPonte_){ compteur_++;}


//---------------------------------
// Getter de l'abscisse
//---------------------------------
int Individu::getX() {
    return x_;
}

//---------------------------------
// Getter de l'ordonnée
//---------------------------------
int Individu::getY() {
    return y_;
}

//---------------------------------
// Getter de points de vie
//---------------------------------
int Individu::getPointsDeVie() {
    return pointsDeVie_;
}


int Individu::getAge(){
    return age_;
}

//---------------------------------
// Setter du pointeur vers l'adversaire
//---------------------------------
void Individu::setAdversaire(Individu * i){
    adversaire_=i;
}

//---------------------------------
// Setter de l'attribut tempoPonte_
//---------------------------------
void Individu::setTempoPonte(int t) {
    tempoPonte_ = t;
}

//---------------------------------
// Set l'attribut porteOeuf_ à son contraire
//---------------------------------
void Individu::switchPorteOeuf() {
    porteOeuf_ = !porteOeuf_;
}

//---------------------------------
// Set l'attribut conflit_ à true
//---------------------------------
void Individu::switchConflictOn() {
    conflit_=true;
}

//---------------------------------
// Set l'attribut conflit_ à son contraire
//---------------------------------
void Individu::switchConflictOff() {
    conflit_=false;
}

//---------------------------------
// Procédure de vieillissement
//---------------------------------
void Individu::vieillissement() {
    age_++;
    if(age_%10 == 5 || age_%10 == 0){
        attaque_+=age_/10;
    }
}

//---------------------------------
// Procédure de test pour la reproductibilité
//---------------------------------
bool Individu::isReproductible() {
    return (age_>=50 && !porteOeuf_ && tempoPonte_==0);
}


//---------------------------------
//  Procédure de détermination de la direction
//---------------------------------
void Individu::direction(int dirx, int diry, int sens, std::vector<std::vector<Individu*>> map){
    int             prevX=x_;
    int             prevY=y_;


    // Cas direction aleatoire
    if(dirx == -1 || diry == -1){
        if (MtRandReal()<0.5) x_++;
        else x_--;

        if(MtRandReal()<0.5) y_--;
        else y_++;
    }
    // Cas direction dans la direction de (sens = 1)
    else if(sens >0){
        if(dirx - x_ >1) x_++;
        else x_--;
        if(diry -y_>1) y_++;
        else y_--;
    }
    // Cas direction opposée à (sens = -1)
    else {
        if(dirx - x_ >0) x_--;
        else x_++;
        if(diry -y_>0) y_--;
        else y_++;
    }

    // Si les coordonnées sortent de la map, on ne bouge pas
    if(x_<0 || x_>=(int) map.size()){
        x_=prevX;
    }
    if(y_<0 || y_>=(int) map[0].size()){
        y_=prevY;
    }

    // Si l'individu sort de la map, il reste sur le bord
    if(x_<0) x_++;
    if(y_<0) y_++;
    if(x_>= (int)map.size()) x_--;
    if(y_>= (int)map[0].size()) y_--;

}



//---------------------------------
//  Fonction d'iteration d'un individu
//      Effectue l'action nécessaire à l'iteration d'un individu
//      Retourne la nouvelle position de l'individu
//---------------------------------
std::pair<int,int> Individu::iteration(std::vector<std::vector<Individu*>> map, std::vector<std::vector<Individu*>> nextMap) {
    // On renverra les coordonnées de l'état t+1
    std::pair<int,int>          pair;

    // On diminue le tempo de ponte s'il est non nul
    if(tempoPonte_ > 0){
        tempoPonte_--;
    }

    // Si on est mort
    if(pointsDeVie_<=0){
        //std::cout<<"Mort d'un type "<<this->getType()<< " par KO"<<std::endl;
        pair.first=-1;
        pair.second=-1;
    }
    // Si on est trop vieux
    else if(age_>=500){
        //std::cout<<"Mort d'un type "<<this->getType()<<" de vieilliesse"<<std::endl;
        pair.first=-1;
        pair.second=-1;
    }
    // Si  l'individu est en conflit
    else if(conflit_){
        //std::cout<<"Conflit"<<std::endl;
        // Frapper l'adversaire
        resoudreConflit();
        pair.first=x_;
        pair.second=y_;
    }
    // Sinon
    else{
        // Se deplacer
        // Tourne jusqu'à ce qu'une case non vide soit trouvée
        int i=0;
        do {
            i++;
            //std::cout<<"Deplacement"<<std::endl;
            pair = deplacement(map, nextMap);
        }while(nextMap[pair.first][pair.second]!=NULL && i!=50); // Limitation à 50 itération, si jamais ...
    }

    return pair;
}


//---------------------------------
//  Fonction de détermination de l'état de santé
//---------------------------------
bool Individu::estEnPleineForme() {
    return (pointsDeVie_>(20*MAX_PV)/100);
}


//---------------------------------
//  Procédure simulant une attaque
//      Retire le nombre de points de vie souhaité
//---------------------------------
void Individu::subirDegats(int d){
    pointsDeVie_-= d;
}


//---------------------------------
//  Procédure de gestion de conflit
//      Soustrait les points de vie
//      Gère le cas où l'ennemi meurt en désactivant le conflit et en supprimant le pointeur
//---------------------------------
void Individu::resoudreConflit() {
    adversaire_->subirDegats(attaque_);

    if(adversaire_->getPointsDeVie()<=0) {
        conflit_ = false;
        adversaire_->switchConflictOff();
        adversaire_->setAdversaire(NULL);
        adversaire_ = NULL;
    }
}


//---------------------------------
//  Procédure d'affichage d'info de l'individu
//---------------------------------
void Individu::getInfo() {
    std::cout<<"Coordonnées | x : "<<x_<<" y : "<<y_<<std::endl;
    std::cout << "Conflit : " << conflit_ << " Porte oeuf : " << porteOeuf_ << " | Tempo ponte : " << tempoPonte_ << std::endl;
    std::cout<<"Adversaire : "<<(adversaire_!=NULL)<<std::endl;
    std::cout<<"PdV : "<<pointsDeVie_<<" Attaque : "<<attaque_<<" Age : "<<age_<<std::endl;
}




//---------------------------------
//  Destructeur
//---------------------------------
Individu::~Individu() {
    compteur_--;
}

