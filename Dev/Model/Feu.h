/*!  Classe Feu - Instance d'agent de type Feu*/
/**
* \file Feu.hpp
* \brief Descriptif de la classe d'agent de type Feu
* \author Justin Goutey & Maxime Sibellas
* \date 15 décembre 2015
* \addtogroup Simulation
*/

#ifndef FEU_H
#define FEU_H

#include "Individu.h"


/*! \class Feu
    \brief Classe d'instance de type Feu
*/

class Feu: public Individu{
protected :
    static int      xNid_; //!< Abscisse du nid
    static int      yNid_; //!< Ordonnée du nid

public :

    //! Constructeur par défaut
    /*!
     *  N'est jamais utilisé.
     *  \sa Feu(int,int,int,int,int), Feu(const Feu&)
     */
    Feu();


    //! Constructeur spécialisé
    /*!
     *  \param x Abscisse initiale
     *  \param y Ordonnee initiale
     *  \param pdv Valeur de points de vie
     *  \param atk Valeur d'attaque
     *  \param age Valeur d'age
     *  \sa Feu(), Feu(const Feu&)
     */
    Feu(int,int,int,int,int);

    //! Constructeur par copie
    /*!
     *  \param f Entité à copier
     *  \sa Feu(), Feu(int,int,int,int,int)
     */
    Feu(const Feu&);

    //! Getter de l'attribut de classe de l'abscisse du nid
    /*!
     *  \return Abscisse du nid
     *  \sa getYNid
     */
    static int getXNid();

    //! Setter de l'attribut de classe de l'abscisse du nid
    /*!
     * \param x Nouvelle abscisse du nid
     * \sa setYNid
     */
    static void setXNid(int);

    //! Getter de l'attribut de classe de l'ordonnée du nid
    /*!
     * \return Ordonnée du nid
     * \sa getXNid
     */
    static int getYNid();

    //! Setter de l'attribut de classe de l'ordonnée du nid
    /*!
     * \param y Nouvelle ordonnée du nid
     * \sa setXNid
     */
    static void setYNid(int);


    //! Fonction de test pour la création d'un nouvel individu
    /*!
     * \return Résultat du test
     */
    bool isPondable();

    //! Procédure de détection d'ennemi
    /*!
     * \param map Map sur laquelle se déplace l'entité
     *
     * \return Nouveaux coordonnées de l'entité
     * \sa presenceAllie, deplacement
     */
    std::pair<int,int> presenceEnnemi(std::vector<std::vector<Individu*>>);

    //! Procédure de détection d'allie
    /*!
     * \param map Map sur laquelle se déplace l'entité
     *
     * \return Nouveaux coordonnées de l'entité
     * \sa presenceEnnemi, deplacement
     */
    std::pair<int,int> presenceAllie(std::vector<std::vector<Individu*>>);


    //! Fonction de déplacement d'une entité
    /*!
     * \param map Map sur laquelle se déplace l'entité
     * \param nextMap Map sur laquelle est projetée l'entité à t+1
     *
     * \return Nouvelles coordonnées de l'entité
     * \sa presenceAllie, presenceEnnemi
     */
    std::pair<int,int> deplacement(std::vector<std::vector<Individu*>>,std::vector<std::vector<Individu*>>);


    //! Procédure de récupération du type
    /*!
     * \return Valeur du type
     * \sa getXNid, getYNid
     */
    std::string getType();

    //! Destructeur
    /*!
     *
     */
    ~Feu();
};

#endif
