
/**
* \file Gestionnaire.cpp
* \author Justin Goutey & Maxime Sibellas
* \date 8 février 2016
* \brief Déclaration des méthodes de la classe Gestionnaire
* \addtogroup Simulation
*/

#include <cstring>
#include <sstream>
#include <fstream>
#include <numeric>

#include "Gestionnaire.h"


int Gestionnaire::compteur_=0;

//---------------------------------
//  Constructeur par défault
//      Crée deux individus
//---------------------------------
Gestionnaire::Gestionnaire() : nbFeu_(1), nbGlace_(1), feu_(0), glace_(0)
{
    compteur_++;
    // Construction des maps vides
    for(int i=0;i<20;i++){
        carte_.push_back(std::vector<Individu*>());
        nextMap_.push_back(std::vector<Individu*>());
        for(int j=0;j<20;j++){
            carte_[i].push_back(NULL);
            nextMap_[i].push_back(NULL);
        }
    }

    feu_.push_back(new Feu(3,3,(int)MtRandInt()%MAX_PV,(int)MtRandInt()%MAX_ATK,0));
    carte_[3][3]=feu_.back();
    glace_.push_back(new Glace(3,2,(int)MtRandInt()%MAX_PV,(int)MtRandInt()%MAX_ATK,0));
    carte_[3][2]=glace_.back();

}

//---------------------------------
//  Constructeur avec paramétrage de la map
//      Crée deux individus
//---------------------------------
Gestionnaire::Gestionnaire(int x, int y) : nbFeu_(2), nbGlace_(0), feu_(0), glace_(0)
{
    compteur_++;

    Glace::setXNid(x - 5);
    Glace::setYNid(y - 5);

    for(int i=0; i < x; i++){
        carte_.push_back(std::vector<Individu*>());
        nextMap_.push_back(std::vector<Individu*>());
        for(int j=0; j < y; j++){
            carte_[i].push_back(NULL);
            nextMap_[i].push_back(NULL);
        }
    }
    feu_.push_back(new Feu(3,3,(int)MtRandInt()%MAX_PV,(int)MtRandInt()%MAX_ATK,0));
    carte_[3][3]=feu_.back();
    feu_.push_back(new Feu(3,2,(int)MtRandInt()%MAX_PV,(int)MtRandInt()%MAX_ATK,0));
    carte_[3][2]=feu_.back();

}


//---------------------------------
//  Constructeur complet
//      Crée les individus proches du nid
//---------------------------------
Gestionnaire::Gestionnaire(int nbGlace, int nbFeu, int abscisseMap, int ordonneeMap) : nbFeu_(nbFeu), nbGlace_(nbGlace), feu_(0), glace_(0)
{
    int             x,
                    y,
                    i=0;

    assert((nbFeu+nbGlace)<=(abscisseMap+ordonneeMap) && "trop d'individu pour la carte");
    compteur_++;


    // Definition des positions des nids
    Glace::setXNid(abscisseMap-abscisseMap/7);
    Glace::setYNid(ordonneeMap-ordonneeMap/7);
    Feu::setXNid(abscisseMap/7);
    Feu::setYNid(ordonneeMap/7);

    // Allocation des maps
    for(int i=0; i < abscisseMap; i++){
        carte_.push_back(std::vector<Individu*>());
        nextMap_.push_back(std::vector<Individu*>());
        for(int j=0; j < ordonneeMap; j++){
            carte_[i].push_back(NULL);
            nextMap_[i].push_back(NULL);
        }
    }

    // Génération d'un individu et vérification de non superposition au démarrage
    while(i<nbGlace){
        x = (int)MtRandInt()%abscisseMap;
        y = (int)MtRandInt()%ordonneeMap;
        if(carte_[x][y]==NULL) {
            glace_.push_back(new Glace(x,y, (int) MtRandInt() % MAX_PV, (int) MtRandInt() % MAX_ATK, 0));
            carte_[x][y]=glace_.back();
            i++;
        }
    }

    i =0;

    while(i<nbFeu){
        x = (int)MtRandInt()%abscisseMap;
        y = (int)MtRandInt()%ordonneeMap;
        if(carte_[x][y]==NULL) {
            feu_.push_back(new Feu(x,y, (int) MtRandInt() % MAX_PV, (int) MtRandInt() % MAX_ATK, 0));
            carte_[x][y]=feu_.back();
            i++;
        }
    }

}


//---------------------------------
//  Getter de l'attribut compteur_
//---------------------------------
int Gestionnaire::getCompteur() {
    return compteur_;
}


//---------------------------------
//  Getter de l'attribut feu_
//---------------------------------
std::vector<Feu *> Gestionnaire::getFeu(){
    return feu_;
}

//---------------------------------
//  Getter de l'attribut glace_
//---------------------------------
std::vector<Glace *> Gestionnaire::getGlace(){
    return  glace_;
}


//---------------------------------
//  Getter du parametre carte_
//---------------------------------
std::vector<std::vector<Individu*>> Gestionnaire::getCarte(){
    return carte_;
}


//---------------------------------
//  Procédure de mise à jour de la map
//      Copie la map(t+1) dans la map(t)
//      Vide la map(t+1)
//---------------------------------
void Gestionnaire::miseAJour(){
    carte_=nextMap_;
    // Nettoyage de la map à l'instant t+1
    for(std::size_t i=0;i<nextMap_.size();i++){
        for(std::size_t j=0; j<nextMap_[i].size();j++){
            nextMap_[i][j]=NULL;
        }
    }
}


//---------------------------------
//  Procédure d'itération du system
//      Itere chacun des individus
//---------------------------------
void Gestionnaire::iteration(int iter){
    std::pair<int,int>      pair;

    mortsF_.push_back(0);
    mortsG_.push_back(0);
    naissancesF_.push_back(0);
    naissancesG_.push_back(0);

    // Récupération des valeurs précédentes pour les naissances et morts
    if(iter !=0){
        naissancesG_[iter]=naissancesG_[iter-1];
        naissancesF_[iter]=naissancesF_[iter -1];
        mortsG_[iter]=mortsG_[iter -1];
        mortsF_[iter]=mortsF_[iter-1];
    }

    for(std::size_t i=0;i<feu_.size();i++){
        pair = feu_[i]->iteration(carte_,nextMap_);
        if(pair.first==-1){
            feu_.erase(feu_.begin()+i);
            // Calcul des morts cumulées
            mortsF_[iter]++;

        }
        else{
            nextMap_[pair.first][pair.second]=feu_[i];
            if(feu_[i]->isPondable()){
                feu_.push_back(new Feu(Feu::getXNid(),Feu::getYNid(),(int)MtRandInt()%MAX_PV,(int)MtRandInt()%MAX_ATK,0));
                nextMap_[feu_.back()->getX()][feu_.back()->getY()];
                feu_[i]->switchPorteOeuf();
                // Calcule des naissances cumulées
                naissancesF_[iter]++;
            }
            // On prend un tour
            feu_[i]->vieillissement();
        }
    }

    for(std::size_t i=0;i<glace_.size();i++){
        // Realisation de l'itération de l'individu
        pair = glace_[i]->iteration(carte_,nextMap_);

        // Si résultat -1, alors destruction de l'individu (mort)
        if(pair.first==-1){
            glace_.erase(glace_.begin()+i);
            // Calcul des morts cumulées
            mortsG_[iter]++;
        }
        // Sinon MaJ de la map à t+1
        else {
            nextMap_[pair.first][pair.second] = glace_[i];
            if(glace_[i]->isPondable()){
                glace_.push_back(new Glace(Glace::getXNid(),Glace::getYNid(),(int)MtRandInt()*MAX_PV,(int)MtRandInt()*MAX_ATK,0));
                nextMap_[glace_.back()->getX()][glace_.back()->getY()];
                glace_[i]->switchPorteOeuf();
                // Calcul des naissances cumulées
                naissancesG_[iter]++;
            }
            glace_[i]->vieillissement();
        }
    }
}


//---------------------------------
//  Procédure d'affichage de la map
//---------------------------------
void Gestionnaire::afficher(){
    int abscisse = (int)carte_.size();
    int ordonnee = (int)carte_[0].size();

    printf("\t//");

    for (int i = 0;i<ordonnee;i++)
    {
        printf("//");
    }

    printf("///\n");

    for (int i = 0;i<abscisse;i++)
    {
        printf("\t// ");
        for (int j = 0;j<ordonnee;j++)
        {
            if(carte_[i][j]==NULL){
                printf("- ");
            }
            else if(carte_[i][j]->getType()=="Feu"){
                if(carte_[i][j]->getAge()<20){
                    printf("f ");
                }
                else {
                    printf("F ");
                }
            }
            else{
                if(carte_[i][j]->getAge()<20){
                    printf("g ");
                }
                else {
                    printf("G ");
                }
            }
        }
        printf("//\n");
    }

    printf("\t//");
    for (int i = 0;i<ordonnee;i++)
    {
        printf("//");
    }
    printf("///\n\n");
}


//---------------------------------
//  Procédure de déroulement de la simulation
//      Effectue n iteration
//---------------------------------
void Gestionnaire::deroulement(int iter){
    for(int i =0; i < iter; i++){

        std::cout<<"############################################"<<std::endl;
        std::cout<<"Itération n°"<<i+1<<std::endl;
        std::cout<<"############################################"<<std::endl;

        std::cout<<std::endl;
        std::cout<<std::endl;

        iteration(i);
        miseAJour();
        //getInfo();
        afficher();
    }
}


//---------------------------------
//  Procédure d'affichage d'infos du system
//---------------------------------
void Gestionnaire::getInfo() {

    if(feu_.size()!=0) std::cout<<"Individus Feu"<<std::endl;
    for(std::size_t i=0;i<feu_.size();i++){
        std::cout<<"--------------------------------------------"<<std::endl;
        std::cout<<"Individu n°"<<i+1<<"/"<<feu_.size()<<std::endl;
        std::cout<<"--------------------------------------------"<<std::endl;
        feu_[i]->getInfo();
    }
    std::cout<<"--------------------------------------------"<<std::endl;
    std::cout<<"--------------------------------------------"<<std::endl;
    if(glace_.size()!=0) std::cout<<"Individus Glace"<<std::endl;
    for(std::size_t i=0;i<glace_.size();i++){
        std::cout<<"--------------------------------------------"<<std::endl;
        std::cout<<"Individu n°"<<i+1<<"/"<<glace_.size()<<std::endl;
        glace_[i]->getInfo();
        std::cout<<"--------------------------------------------"<<std::endl;
    }
    std::cout<<std::endl;
}


//---------------------------------
//  Procédure de sauvegarde de la matrice de présence en fichier CSV
//      Cumule les valeurs de présences
//---------------------------------
void Gestionnaire::sauvegarde(std::string fileFeu, std::string fileGlace) {

    // Récupération de la map précédente
    std::vector<std::vector<int>>   prevGlace = restitution(fileGlace);
    std::vector<std::vector<int>>   prevFeu = restitution(fileFeu);
    // Ouverture des streams d'écriture
    std::ofstream                   openFileFeuW(fileFeu, std::ios::trunc);
    std::ofstream                   openFileGlaceW(fileGlace, std::ios::trunc);

    assert(!openFileGlaceW.fail() && "fichier glace non ouvert - Save matrice");
    assert(!openFileFeuW.fail() && "fichier feu non ouvert - Save matrice");


    if(!openFileFeuW.fail() && !openFileGlaceW.fail()){
        for(std::size_t i=0;i<carte_.size();i++) {
            for (std::size_t j = 0; j < carte_[i].size(); j++) {
                // Si aucune présence sur la carte, on garde la valeur de la map précédente
                if (carte_[i][j] == NULL) {
                    openFileFeuW << prevFeu[i][j];
                    openFileGlaceW<< prevGlace[i][j];
                    if (j != carte_[i].size() - 1) {
                        openFileFeuW << ";";
                        openFileGlaceW<<";";
                    }
                    else {
                        openFileFeuW <<";"<< std::endl;
                        openFileGlaceW<<";"<<std::endl;
                    }
                }
                // Si présence d'un individu Feu, on augmente de 1 la valeur de la map feu
                else if (carte_[i][j]->getType() == "Feu") {
                    openFileFeuW << prevFeu[i][j]+1;
                    openFileGlaceW<<prevGlace[i][j];
                    if (j != carte_[i].size() - 1) {
                        openFileFeuW << ";";
                        openFileGlaceW<<";";
                    }
                    else {
                        openFileFeuW <<";"<< std::endl;
                        openFileGlaceW<<";"<<std::endl;
                    }
                }
                // Si présence d'un individu Glace, on augmente de 1 la valeur de la map glace
                else {
                    openFileFeuW << prevFeu[i][j];
                    openFileGlaceW<<prevGlace[i][j]+1;
                    if (j != carte_[i].size() - 1) {
                        openFileFeuW << ";";
                        openFileGlaceW << ";";
                    }
                    else {
                        openFileFeuW <<";"<< std::endl;
                        openFileGlaceW<<";"<<std::endl;
                    }
                }
            }
        }

        openFileFeuW.close();
        openFileGlaceW.close();
    }
}


//---------------------------------
//  Procédure de sauvegarde des données démographiques en fichier CSV
//      Comprend deux lignes :
//          Les naissances cumulées
//          Les morts cumulées
//---------------------------------
void Gestionnaire::saveDemographie(std::string fileFeu, std::string fileGlace,int iter) {

    // Récupération des données de la simulation précédente
    std::vector<std::vector<int>>   prevResult=restitutionDemo(fileFeu,fileGlace, iter);

    std::ofstream                   openFileFeuW(fileFeu, std::ios::trunc);
    std::ofstream                   openFileGlaceW(fileGlace, std::ios::trunc);

    assert(!openFileGlaceW.fail() && "fichier glace non ouvert - Save demo");
    assert(!openFileFeuW.fail() && "fichier feu non ouvert - Save demo");

    if(!openFileFeuW.fail() && ! openFileGlaceW.fail()){
        // Premier ligne fichier glace : naissances
        for(std::size_t i=0; i < naissancesG_.size(); i++){
            openFileGlaceW <<naissancesG_[i]+prevResult[0][i] <<";";
        }
        openFileGlaceW<<std::endl;
        // Deuxieme ligne fichier glace : morts
        for(std::size_t i=0; i < mortsG_.size(); i++){
            openFileGlaceW <<mortsG_[i]+prevResult[1][i] <<";";
        }
        // Premier ligne fichier feu : naissances
        for(std::size_t i=0; i < naissancesF_.size(); i++){
            openFileFeuW << naissancesF_[i]+prevResult[2][i] << ";";
        }
        openFileFeuW<<std::endl;
        // Deuxieme ligne fichier feu : morts
        for(std::size_t i=0; i < mortsF_.size(); i++){
            openFileFeuW << mortsF_[i]+prevResult[3][i] << ";";
        }

        openFileFeuW.close();
        openFileGlaceW.close();
    }

}

//---------------------------------
//  Procédure de récupération d'un fichier CSV contenant une matrice
//---------------------------------
std::vector<std::vector<int>> Gestionnaire::restitution(std::string file){

    std::ifstream                   openFile(file);
    std::string                     s;
    std::vector<std::vector<int>>   v;
    int                             id=0, idd=0;

    if(!openFile.fail()) {
        // Si le fichier existe
        do {
            // On crée une ligne
            v.push_back(std::vector<int>());
            // On récupère la ligne dans le fichier
            while(idd<(int)carte_.size() && getline(openFile, s, ';')){

                // s plus longue que 1 carac
                if (s.size()>1){
                    // s : changement de ligne
                    if(s.c_str()[0] == '\n'){
                        s.erase(s.begin());
                        v[id].push_back(atoi(s.c_str()));
                    }
                    else{
                        v[id].push_back(atoi(s.c_str()));
                    }
                }
                else{
                    v[id].push_back(atoi(s.c_str()));
                }

                idd++;
            }
            idd=0;
            // On remplis le vector ( en omettant les séparateurs )
            id++;
        }
        while(openFile.good());
        openFile.close();

    }
    else {
        // Si le fichier n'existe pas, on renvoi une matrice nulle
        for (std::size_t i = 0; i < carte_.size(); i++) {
            v.push_back(std::vector<int>());
            for (std::size_t j = 0; j < carte_[i].size(); j++) {
                v[i].push_back(0);
            }
        }
    }
    return v;
}



//---------------------------------
//  Procédure de récupération des données démographiques à partir d'un fichier CSV
//---------------------------------
std::vector<std::vector<int>> Gestionnaire::restitutionDemo(std::string fileFeu, std::string fileGlace, int iter){

    std::ifstream                   openFileF(fileFeu);
    std::ifstream                   openFileG(fileGlace);
    std::string                     s;
    std::vector<std::vector<int>>   v;
    int                             id=0, idd=0;

    if(!openFileG.fail()) {
        // Si le fichier existe
        while(openFileG.good()){
            // On crée une ligne
            v.push_back(std::vector<int>());
            // On récupère la ligne dans le fichier
            while(idd<iter && getline(openFileG, s, ';')){

                // s plus longue que 1 carac
                if (s.size()>1){
                    // s : changement de ligne
                    if(s.c_str()[0] == '\n'){
                        s.erase(s.begin());
                        v[id].push_back(atoi(s.c_str()));
                    }
                    else{
                        v[id].push_back(atoi(s.c_str()));
                    }
                }
                else{
                    v[id].push_back(atoi(s.c_str()));
                }

                idd++;
            }
            idd=0;
            // On remplis le vector ( en omettant les séparateurs )
            id++;
        }

        openFileG.close();
    }
    else {
        // Si le fichier n'existe pas, on renvoi une matrice nulle
        for (int i = 0; i < 2; i++) {
            v.push_back(std::vector<int>());
            for (int j = 0; j < iter; j++) {
                v[i].push_back(0);
            }
        }
    }

    if(v.size()>2){
        v.erase(v.end()-1);
        id--;
    }

    if(!openFileF.fail()) {
        // Si le fichier existe
        do {
            // On crée une ligne
            v.push_back(std::vector<int>());
            // On récupère la ligne dans le fichier
            while(idd<iter && getline(openFileF, s, ';')){

                // s plus longue que 1 carac
                if (s.size()>1){
                    // s : changement de ligne
                    if(s.c_str()[0] == '\n'){
                        s.erase(s.begin());
                        v[id].push_back(atoi(s.c_str()));
                    }
                    else{
                        v[id].push_back(atoi(s.c_str()));
                    }
                }
                else{
                    v[id].push_back(atoi(s.c_str()));
                }

                idd++;
            }
            idd=0;
            // On remplis le vector ( en omettant les séparateurs )
            id++;
        }
        while(openFileF.good());
        openFileF.close();
    }
    else {
        // Si le fichier n'existe pas, on renvoi une matrice nulle
        for (int i = 2; i < 4; i++) {
            v.push_back(std::vector<int>());
            for (int j = 0; j < iter; j++) {
                v[i].push_back(0);
            }
        }
    }


    return v;
}




//---------------------------------
//  Destructeur
//---------------------------------
Gestionnaire::~Gestionnaire() {
    for(std::size_t i=0;i<feu_.size();i++){
        delete(feu_[i]);
    }
    feu_.clear();
    for (std::size_t j = 0; j < glace_.size(); j++) {
        delete(glace_[j]);
    }
    glace_.clear();

    for(std::size_t i=0;i<carte_.size();i++){
        carte_[i].clear();
        nextMap_[i].clear();
    }
    carte_.clear();
    nextMap_.clear();
    compteur_--;
}


