

/**
* \file Glace.cpp
* \brief Déclaration des méthodes de la classe d'agent de type Glace
* \author Justin Goutey & Maxime Sibellas
* \date 15 décembre 2015
* \addtogroup Simulation
*/


#include "Glace.h"

int Glace::xNid_=20;
int Glace::yNid_=20;


//---------------------------------
//  Constructeur par défaut
//---------------------------------
Glace::Glace(): Individu() {}


//---------------------------------
//  Constructeur spécialisé
//---------------------------------
Glace::Glace(int x, int y, int pdv, int atk, int age) : Individu(x, y, pdv, atk, age) {

}


//---------------------------------
//  Constructeur par copie
//---------------------------------
Glace::Glace(const Glace&G) : Individu(G.x_, G.y_, G.pointsDeVie_, G.attaque_, G.age_) {

}

//---------------------------------
//  Constructeur par copie
//---------------------------------
int Glace::getXNid(){
    return xNid_;
}

//---------------------------------
//  Constructeur par copie
//---------------------------------
void Glace::setXNid(int x){
    xNid_= x;
}

//---------------------------------
//  Constructeur par copie
//---------------------------------
int Glace::getYNid(){
    return yNid_;
}

//---------------------------------
//  Constructeur par copie
//---------------------------------
void Glace::setYNid(int y){
    yNid_= y;
}


//---------------------------------
//  Fonction de test pour la création d'un nouvel individu
//---------------------------------
bool Glace::isPondable(){
    return(porteOeuf_ && x_<=Glace::xNid_+2 && x_>=Glace::xNid_-2 && y_<=Glace::yNid_+2 && y_>=Glace::yNid_-2 );
}


//---------------------------------
//  Fonction de détection d'un ennemi
//      Renvoi les coordonnées de l'ennemi
//          (-1,-1) si absence d'ennemi
//      Active le conflit si l'ennemi est au contact
//---------------------------------
std::pair<int,int> Glace::presenceEnnemi(std::vector<std::vector<Individu*>> map) {
    std::pair<int,int>  pair;
    int                 x=-1;
    int                 y=-1;

    for(int i=std::max(x_-2,0);i<=std::min(x_+2, (int) map.size() - 1); i++){
        for(int j=std::max(y_-2,0);j<=std::min(y_+2, (int) map[0].size() - 1); j++){
            if(map[i][j] != NULL && map[i][j] != map[x_][y_] && map[i][j]->getType() == "Feu"){
                // Si ennemi au contact
                if((i<=x_+1 && i>=x_-1) && (j<=y_+1 && j>=y_-1)){
                    conflit_=true;
                    adversaire_= map[i][j];
                    map[i][j]->setAdversaire(this);
                    map[i][j]->switchConflictOn();
                }
                x=i;
                y=j;
            }
        }
    }
    pair = std::make_pair(x,y);
    return pair;

}


//---------------------------------
//  Fonction de détection d'un allie
//      Renvoi les coordonnées de l'allie
//          (-1,-1) si absence de l'allie
//---------------------------------
std::pair<int,int> Glace::presenceAllie(std::vector<std::vector<Individu*>> map) {
    std::pair<int,int>  pair;
    int                 x=-1;
    int                 y=-1;

    for(int i=std::max(x_-2,0);i<=std::min(x_+2, (int) map.size() - 1); i++){
        for(int j=std::max(y_-2,0);j<=std::min(y_+2, (int) map[0].size() - 1); j++){
            // S'il y a un allié et qu'il est plus proche qu'un précédent allié enregistré
            if(map[i][j] != NULL && map[i][j] != map[x_][y_] && map[i][j]->getType() == "Glace"){
                // S'il est au contact et qu'il peut y avoir reproduction
                if((i<=x_+1 && i>=x_-1) && (j<=y_+1 && j>=y_-1) && isReproductible() && map[i][j]->isReproductible()){
                    porteOeuf_=true;
                    tempoPonte_ =20;
                    map[i][j]->setTempoPonte(20); // On applique la tempo à l'autre individu
                }
                x=i;
                y=j;
            }
        }
    }
    pair= std::make_pair(x,y);
    return pair;
}



//---------------------------------
//  Fonction de déplacement d'un individu
//      Retourne la nouvelle position de l'individu
//---------------------------------
std::pair<int,int> Glace::deplacement(std::vector<std::vector<Individu*>> map, std::vector<std::vector<Individu*>> nextMap) {

    std::pair<int,int>      coordEnnemi = presenceEnnemi(map),
                            coordAllie = presenceAllie(map),
                            coordNid = std::make_pair(xNid_,yNid_);
    bool                    bEnemy =(coordEnnemi.first!=-1),
                            bAllie =(coordAllie.first!=-1);
    double                  rand = MtRandReal();

    if(conflit_){
        resoudreConflit();
    }
    // Faible OU jeune OU porteur d'oeuf
    else if(!estEnPleineForme() || age_<5 || porteOeuf_){
        if(bAllie && bEnemy){
            if(rand<=0.1){
                direction(-1, -1, 1, nextMap);
            }
            else if(rand <= 0.2){
                direction(coordEnnemi.first, coordEnnemi.second, -1, nextMap);
            }
            else if (rand <= 0.5){
                direction(coordAllie.first, coordAllie.second, 1, nextMap);
            }
            else{
                direction(coordNid.first, coordNid.second, 1, nextMap);
            }
        }
        else if(bAllie && !bEnemy){
            if(rand<=0.25){
                direction(-1, -1, 1,nextMap);
            }
            else if (rand <= 0.6){
                direction(coordAllie.first, coordAllie.second, 1,nextMap);
            }
            else{
                direction(coordNid.first, coordNid.second, 1,nextMap);
            }
        }
        else if (!bAllie && bEnemy){
            if(rand<=0.2){
                direction(-1, -1, 1, nextMap);
            }
            else if(rand <= 0.45){
                direction(coordEnnemi.first, coordEnnemi.second, -1,nextMap);
            }
            else{
                direction(coordNid.first, coordNid.second, 1, nextMap);
            }
        }
        else {
            if(rand<=0.35){
                direction(-1, -1, 1, nextMap);
            }
            else{
                direction(coordNid.first, coordNid.second, 1, nextMap);
            }
        }
    }
        //Non porteur d'oeuf ET periode de reproduction
    else if(!porteOeuf_ ){
        if(bAllie && bEnemy){
            if(rand<=0.2){
                direction(-1, -1, 1, nextMap);
            }
            else if(rand <= 0.5){
                direction(coordEnnemi.first, coordEnnemi.second, -1, nextMap);
            }
            else{
                direction(coordAllie.first, coordAllie.second, 1, nextMap);
            }
        }
        else if(bAllie && !bEnemy){
            if(rand<=0.4){
                direction(-1, -1, 1, nextMap);
            }
            else{
                direction(coordAllie.first, coordAllie.second, 1, nextMap);
            }
        }
        else if (!bAllie && bEnemy){
            if(rand<=0.4){
                direction(-1, -1, 1, nextMap);
            }
            else{
                direction(coordEnnemi.first, coordEnnemi.second, -1, nextMap);
            }
        }
        else {
            direction(-1, -1, 1, nextMap);
        }
    }
        // En pleine forme ET non période de reproduction
    else if(estEnPleineForme() ){
        if(bAllie && bEnemy){
            if(rand<=0.1){
                direction(-1, -1, 1, nextMap);
            }
            else if(rand <= 0.4){
                direction(coordEnnemi.first, coordEnnemi.second, -1, nextMap);
            }
            else{
                direction(coordAllie.first, coordAllie.second, 1, nextMap);
            }
        }
        else if(bAllie && !bEnemy){
            if(rand<=0.4){
                direction(-1, -1, 1,nextMap);
            }
            else{
                direction(coordAllie.first, coordAllie.second, 1, nextMap);
            }
        }
        else if (!bAllie && bEnemy){
            if(rand<=0.25){
                direction(-1, -1, 1, nextMap);
            }
            else{
                direction(coordEnnemi.first, coordEnnemi.second, -1,nextMap);
            }
        }
        else {
            direction(-1, -1, 1, nextMap);
        }
    }
    else{
        direction(-1, -1, 1, nextMap);
    }
    return std::make_pair(x_,y_);
}


//---------------------------------
//  Fonction de récupération du type
//---------------------------------
std::string Glace::getType() {
    return "Glace";
}


//---------------------------------
//  Destructeur
//---------------------------------
Glace::~Glace(){

}