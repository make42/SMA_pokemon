/***********************************************/
/**
* \file main.cpp
* \brief Programme principal de la simulation
* \author Justin Goutey & Maxime Sibellas
* \date 8 février 2016
*/
/***********************************************/
/* Documentation tag for Doxygen
 */

/**
 * \mainpage Simulation multi-agent
 *
 * \section intro_sec Introduction
 *
 * Ceci est la documentation du projet de simulation multi-agent.<BR>
 * Projet réalisé par Justin Goutey & Maxime Sibellas.<BR>
 *
 * La simulation consiste en l'étude du comportement et de l'interation entre deux espèces d'agents différentes. Nos agents sont deux espèces de Pokémon de type opposés. Chaque espèce possède une zone "sécurisée" appelée nid dans un coin du terrain qui leur appartient.<BR>
 * L'interaction entre les deux espèces consiste à un conflit violent qui aboutit à la mort d'un des deux individus.<BR>
 * L'interaction entre des agents de la même espèce consiste en un processus de reproduction.<BR>
 *
 * Code source et rapport : <A HREF="https://gitlab.com/make42/SMA_pokemon">ici</A>.
 *
 * \section install_sec Installation & configuration
 * Langage utilisé : C++11.
 *
 * \section running Exécution du programme
 * L'exécution en mode console comprend les paramètres suivant :
 * <UL>
 *      <LI>nF : nombre d'agents initiaux de type feu</LI>
 *      <LI>nG : nombre d'agents initiaux de type glace</LI>
 *      <LI>x : largeur de la carte</LI>
 *      <LI>y : hauteur de la carte</LI>
 *      <LI>nI : nombre d'iteration d'une simulation</LI>
 *      <LI>nS : nombre de simulation souhaité</LI>
 *      <LI>filePath : chemin vers le dossier de sauvegarde des résultats</LI>
 * </UL>
 *
 *
 * \section copyright Copyright and License
 *  Utilisation du Mersenne Twister développé par Makoto Matsumoto & Takuji Nishimura.
 *
 * <BR><BR>
 *
 */


#include <iostream>
#include <sstream>
#include <unistd.h>
#include "Model/Gestionnaire.h"

int main(int argc, char ** argv) {
    initRng(); // Initialisation du mersenne twister

    std::stringstream   iss;
    std::string         fileFeuMatrix,
                        fileGlaceMatrix,
                        fileFeuDemo,
                        fileGlaceDemo,
                        mainDossier="Result/";


    int                 iter = 200,
                        simu = 3,
                        nbFeu=30,
                        nbGlace=30,
                        abs = 50,
                        ord = 50;

    // Paramétrage manuel
    if(argc>=2){
        nbFeu=atoi(argv[1]);
    }
    if(argc>=3){
        nbGlace=atoi(argv[2]);
    }
    if(argc>=4){
        abs=atoi(argv[3]);
    }
    if(argc>=5){
        ord=atoi(argv[4]);
    }
    if(argc>=6){
        iter = atoi(argv[5]);
    }
    if(argc>=7){
        simu = atoi(argv[6]);
    }
    if(argc>=8){
        mainDossier=argv[7];
    }


    // Rajout du '/' en fin de path s'il n'existe pas
    if(mainDossier.at(mainDossier.size()-1)!='/'){
        mainDossier.insert(mainDossier.end(),'/');
    }

    // Formation du nom de la sauvegarde
    iss << mainDossier<<"feu_" << simu << "_" << iter << "_" << abs << "x" << ord << "_" << nbFeu << "f" << nbGlace << "g" << ".csv";
    fileFeuMatrix = iss.str();
    iss.str("");
    iss << mainDossier<<"glace_" << simu << "_" << iter << "_"<<abs<<"x"<<ord<<"_"<<nbFeu<<"f"<<nbGlace<<"g"<<".csv";
    fileGlaceMatrix =iss.str();
    iss.str("");
    iss<<mainDossier<<"Demo_feu_"  << simu << "_" << iter << "_" << abs << "x" << ord << "_" << nbFeu << "f" << nbGlace << "g" << ".csv";
    fileFeuDemo= iss.str();
    iss.str("");
    iss <<mainDossier<<"Demo_glace_" << simu << "_" << iter << "_" << abs << "x" << ord << "_" << nbFeu << "f" << nbGlace << "g" << ".csv";
    fileGlaceDemo=iss.str();

    // Creation du gestionnaire de simulation
    Gestionnaire * g;

    // Déroulement d'une simulation
    for(int i=0;i<simu;i++) {
        // Initialisation du gestionnaire
        if(Gestionnaire::getCompteur()==0) {
            std::cout<<"Simulation n°"<<i<<std::endl;
            g = new Gestionnaire(nbFeu, nbGlace, abs, ord);

            // Deroulement des itérations
            g->deroulement(iter);

            // Sauvegarde
            g->sauvegarde(fileFeuMatrix, fileGlaceMatrix);
            g->saveDemographie(fileFeuDemo, fileGlaceDemo, iter);
            delete (g);

            sleep(3); // Permets de visualiser le résultat
        }
    }


    return 0;
}