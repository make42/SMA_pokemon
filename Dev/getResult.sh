#!/bin/bash

#################################
#
#	Script de récupération de résultat pour la simulation multi agent
#	Lance la séquence de simulations prédéfinie et organise les fichiers résultants
#
#################################


prog="./SMA_EN_COURS_NE_PAS_STOPPER"
declare -a nbIndiv=("15" "30" "50")
declare -a abs=("30" "50" "80")
declare -a iter=("100" "300" "500")
declare -a simu=("25" "50")


make

exec `rm -r Result/`
exec `mkdir Result/`

for j in "${simu[@]}"; do
	for i in "${iter[@]}"; do
		for k in "${nbIndiv[@]}"; do
			for l in "${abs[@]}"; do
				dir="Result/${j:0}_${i:0}_${l:0}x${l:0}_${k:0}f${k:0}g/"
				fic="${dir:0}timing"
				# Creation du dossier si n'existe pas
				if [ ! -d "$dir" ]; then
					mkdir "$dir"
				fi
				
				echo "lancement programme :"
				echo "$prog $k $k $l $l $i $j $dir"
				(time $prog $k $k $l $l $i $j $dir) 2>&1 | more >>$fic

			done
		done
	done
done



