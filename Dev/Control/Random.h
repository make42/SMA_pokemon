//
// Created by maxime on 27/12/15.
//

#ifndef DEV_RANDOM_H
#define DEV_RANDOM_H


/***********************************************/
/**
	\file Random.h
	\brief Liste des prototypes pour le générateur Mersenne Twister
	\author Makoto Matsumoto & Takuji Nishimura
*/
/*! \addtogroup random Random Generator*/
/***********************************************/

#pragma once


void 			init_genrand(unsigned long s);
void 			init_by_array(unsigned long init_key[], int key_length);
unsigned long 	genrand_int32(void);
long 			genrand_int31(void);
double 			genrand_real1(void);
double 			genrand_real2(void);
double 			genrand_real3(void);
double 			genrand_res53(void);

void 			initRng(); // Initialisation du générateur

double 			MtRandReal(); // Génération d'une valeur réelle aléatoire (dans [0;1])
long            MtRandInt(); // Génération d'une valeur entière aléatoire

#endif //DEV_RANDOM_H
