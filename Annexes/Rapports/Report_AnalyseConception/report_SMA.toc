\select@language {french}
\contentsline {chapter}{\numberline {1}Analyse des besoins}{2}
\contentsline {section}{\numberline {1.1}Classes d'agent}{2}
\contentsline {subsection}{\numberline {1.1.1}Pok\IeC {\'e}mon\texttrademark }{2}
\contentsline {paragraph}{Vie et mort\\}{2}
\contentsline {section}{\numberline {1.2}D\IeC {\'e}placements et champ de vision}{3}
\contentsline {section}{\numberline {1.3}Interactions}{3}
\contentsline {subsection}{\numberline {1.3.1}Entre agent de la m\IeC {\^e}me esp\IeC {\`e}ce}{3}
\contentsline {paragraph}{Gestation\\}{3}
\contentsline {subsection}{\numberline {1.3.2}Entre agents oppos\IeC {\'e}s}{3}
\contentsline {chapter}{\numberline {2}Etablissement des r\IeC {\`e}gles}{5}
\contentsline {section}{\numberline {2.1}R\IeC {\`e}gles de d\IeC {\'e}placement et de vision}{5}
\contentsline {paragraph}{R\IeC {\`e}gles de vision\\}{5}
\contentsline {paragraph}{R\IeC {\`e}gles de d\IeC {\'e}placement\\}{5}
\contentsline {section}{\numberline {2.2}R\IeC {\`e}gles d'interactions}{6}
\contentsline {paragraph}{Lors d'un conflit\\}{6}
\contentsline {paragraph}{Lors d'une rencontre avec un individu amical\\}{6}
\contentsline {section}{\numberline {2.3}R\IeC {\`e}gles suppl\IeC {\'e}mentaires}{7}
\contentsline {paragraph}{Influence sur le climat\\}{7}
\contentsline {paragraph}{D\IeC {\'e}tails sur le "nid"\\}{7}
\contentsline {chapter}{\numberline {3}Planification du projet}{8}
