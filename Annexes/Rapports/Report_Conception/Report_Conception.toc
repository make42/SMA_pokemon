\select@language {french}
\contentsline {chapter}{\numberline {1}\IeC {\'E}tat sur l'\IeC {\'e}volution du sujet}{2}
\contentsline {section}{\numberline {1.1}Entit\IeC {\'e}s \& comportements}{2}
\contentsline {section}{\numberline {1.2}R\IeC {\'e}sultats \IeC {\'e}tudi\IeC {\'e}s}{3}
\contentsline {chapter}{\numberline {2}Conception \& impl\IeC {\'e}mentation du projet}{4}
\contentsline {section}{\numberline {2.1}Conception et impl\IeC {\'e}mentation du moteur de la simulation}{4}
\contentsline {subsection}{\numberline {2.1.1}Entit\IeC {\'e} de simulation : le gestionnaire}{4}
\contentsline {subsection}{\numberline {2.1.2}Structure de la simulation : interfa\IeC {\c c}age entre les entit\IeC {\'e}s}{5}
\contentsline {section}{\numberline {2.2}Visualisation des r\IeC {\'e}sultats}{6}
\contentsline {chapter}{\numberline {3}R\IeC {\'e}sultats}{7}
\contentsline {section}{\numberline {3.1}\IeC {\'E}tude des ressources}{7}
\contentsline {subsection}{\numberline {3.1.1}Profiling}{7}
\contentsline {subsection}{\numberline {3.1.2}Valgrind}{8}
\contentsline {section}{\numberline {3.2}R\IeC {\'e}sultats de simulation}{9}
\contentsline {subsection}{\numberline {3.2.1}Analyse spectrale du r\IeC {\'e}sultat}{9}
\contentsline {paragraph}{Simulation n\degre 1}{10}
\contentsline {paragraph}{Simulation n\degre 2}{11}
\contentsline {subsection}{\numberline {3.2.2}Analyse d\IeC {\'e}mographique du d\IeC {\'e}roulement de la simulation}{12}
\contentsline {paragraph}{Simulation n\degre 1\\}{12}
\contentsline {paragraph}{Simulation n\degre 2\\}{12}
\contentsline {chapter}{\numberline {4}Conclusion}{14}
