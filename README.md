# Simulation Multi-Agent de deux populations
### Projet d'Architecture Logiciel Qualité, 2ème année - ISIMA
#### Etudiants : Sibellas Maxime, Goutey Justin
#### Enseignant : Dr. David Hill
---

Simulation
===

Le moteur de la simulation se trouve dans le dossier Dev/.

Technologie
---
Développé en C++ 11. Nécessite g++ 4.7

Compilation
---
Présence d'un Makefile complet dans Dev.

* `make release` compilation avec l'option -O2 
* `make optimised` compilation avec l'option -O3

WARNING : l'utilisation des options de compilation -O2 et -O3 peuvent aboutir à une modification du résultat final.

WARNING : le fichier CMakeList.txt est un fichier auto généré par l'IDE CLion. L'utiliser pour compiler le projet n'aboutira pas à un executable fonctionnel.


Exécution de la simulation
---
L'exécution en mode console comprend les paramètres suivant :

 * nF : nombre d'agents initiaux de type feu
 * nG : nombre d'agents initiaux de type glace
 * x : largeur de la carte
 * y : hauteur de la carte
 * nI : nombre d'iteration d'une simulation
 * nS : nombre de simulation souhaité
 * filePath : chemin vers le dossier de sauvegarde des résultats


Acquisition des données :
---
`getResults.sh` permet d'acquérir les données du modèle. Les résultats sont stockés dans le dossier Dev/Results. Le script effectue les simulations pour les valeurs suivantes :

* Nombre de simulations : 25 et 50
* Nombre d'itérations : 100, 300 et 500
* Dimensions de la map : 30x30, 50x50, 80x80
* Nombre d'individu au départ : 30, 60, 100

Les simulations s'effectuent les unes après les autres et non simultanéments.


Documentation
---
La documentation auto générée par Doxygen est disponible dans le dossier Documentation du projet.


Traitement des données
===

L'interface de traitement de données se trouve dans le dossier DevAnalyse/.

Technologies
---
L'interface d'affichage de l'analyse spectrale est codée en OpenGL 3.3 et SDL 2.

Nécessite les bibliothèques suivantes :
* Glew
* SDL2
* GLM
* openGL 3.3

INSTALLATION
---
* sudo apt-get install freeglut3 freeglut3-dev 
* sudo apt-get install libglew-dev
* sudo apt-get install libglu1-mesa libglu1-mesa-dev
* sudo apt-get install libsdl2-*
* sudo apt-get install libglm-dev

Compilation
---
Utilisation du CMakeList.txt disponible

Execution
---
Prend en paramètre le fichier .csv correspondant à la matrice à afficher.



