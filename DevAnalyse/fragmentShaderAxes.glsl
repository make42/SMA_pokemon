//
//created by Justin GOUTEY on 08/02/2016.
//

#version 330 core

uniform float rgb;

in vec3 claude_bis;
out vec4 color;

const vec4 tabColor[3]=vec4[](
        vec4(1.,0.0,0.,1.),
        vec4(0.,1.,0.,1.),
        vec4(0.,0.0,1.,1.)
);

void main()
{
    color = tabColor[int(rgb)%3];
}
