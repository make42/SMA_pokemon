//
//created by Justin GOUTEY on 08/02/2016.
//

#include <iostream>
#include <fstream>
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <glm/matrix.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <algorithm>
#include <iterator>
#include "Program.hpp"

using namespace std;

#define HEIGHT 1300*9/16
#define WIDTH 1300
int iterat=0;

void calcMatsTranslation(std::fstream & file, std::vector<glm::vec3> & Trans, std::vector<glm::mat3> & Hthie, int & size);

int main(int argc, char ** argv)
{
    //ouverture du fichier matrice:
    assert(argc==2 && "erreur d'arguments, indiquez le fichier à lire");
    std::fstream file(argv[1], std::ios::in);
    assert(!file.fail() && file != nullptr && "fihier invalide !!");

    if(!SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS))
    {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);//1 seul double buffer

        SDL_Window * win = SDL_CreateWindow("glWindow",0,0,WIDTH,HEIGHT, SDL_WINDOW_SHOWN|SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);

        SDL_GLContext context = SDL_GL_CreateContext(win);

        glewExperimental = true;

        if(glewInit())
        {
            return -1;
        }
        (void)glGetError();

        glClearColor(0.0f,0.0f,0.0f,0.0f);

        glEnable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);

        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);


        //definition spectre

        Program progSpectre("vertexShader.glsl","fragmentShader.glsl");
        glUseProgram(progSpectre.getProgram());

        GLuint vaoSpectre;//vertex array object
        glGenVertexArrays(1, &vaoSpectre);
        glBindVertexArray(vaoSpectre);

        GLuint vboSpectre;
        glGenBuffers(1, &vboSpectre);//1 pour un seul vbo
        glBindBuffer(GL_ARRAY_BUFFER, vboSpectre);

        GLfloat vertexCubeSpectre[]={
                -0.5,0.5,0.5,
                0.5,0.5,0.5,
                0.5,-0.5,0.5,
                -0.5,-0.5,0.5,
                -0.5,0.5,-0.5,
                0.5,0.5,-0.5,
                0.5,-0.5,-0.5,
                -0.5,-0.5,-0.5,
        };

        GLuint elementBuffer[]={
                0, 1, 2,
                0, 2, 3,
                4, 5, 6,
                4, 6, 7,
                1, 5, 6,
                1, 6, 2,
                0, 4, 7,
                0, 7, 3,
                0, 1, 5,
                0, 5, 4,
                3, 7, 6,
                3, 6, 2,
        };

        GLuint eltBuf;
        glGenBuffers(1, &eltBuf);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eltBuf);

        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elementBuffer), elementBuffer, GL_STATIC_DRAW);

        glBufferData(GL_ARRAY_BUFFER, sizeof(vertexCubeSpectre), vertexCubeSpectre, GL_STATIC_DRAW);
        //glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), (void*)0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), (void*)0);
        glEnableVertexAttribArray(0);
        /*glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(GLfloat), (void*)(3*sizeof(GLfloat)));
        glEnableVertexAttribArray(1);*/

        glm::mat3 matRot(1.0,0, 0,
                         0, 1.0, 0,
                         0, 0 ,1);

        GLint uni_Matrice = glGetUniformLocation(progSpectre.getProgram(),"matRot");
        glUniformMatrix3fv(uni_Matrice, 1, GL_FALSE, glm::value_ptr(matRot));


        glm::mat3 matRotY(0,0,0,
                          0,0,0,
                          0,0,0);

        GLint uni_Hthie = glGetUniformLocation(progSpectre.getProgram(),"matHthie");
        GLint uni_Trans = glGetUniformLocation(progSpectre.getProgram(),"vecTrans");

        //définition fond

        Program progFond("vertexShaderFond.glsl","fragmentShaderFond.glsl");
        glUseProgram(progFond.getProgram());

        GLuint vaoFond;//vertex array object
        glGenVertexArrays(1, &vaoFond);
        glBindVertexArray(vaoFond);

        GLuint vboFond;
        glGenBuffers(1, &vboFond);//1 pour un seul vbo
        glBindBuffer(GL_ARRAY_BUFFER, vboFond);

        GLfloat Fond[]={
                1.0f, 1.0f,
                -1.0f, 1.0f,
                -1.0f, -1.0f,
                1.0f, 1.0f,
                1.0f, -1.0f,
                -1.0f, -1.0f,
        };

        GLuint elementBufferFond[] = {
                0, 1, 2,
                3, 4, 5,
        };

        GLuint eltBufFond;
        glGenBuffers(1, &eltBufFond);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eltBufFond);

        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elementBufferFond), elementBufferFond, GL_STATIC_DRAW);

        glBufferData(GL_ARRAY_BUFFER, sizeof(Fond), Fond, GL_STATIC_DRAW);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2*sizeof(GLfloat), (void*)0);//coordonnées x, y, z,
        glEnableVertexAttribArray(0);

        //definition axes

        Program progAxes("vertexShaderAxes.glsl","fragmentShaderAxes.glsl");
        glUseProgram(progAxes.getProgram());

        GLuint vaoAxes;
        glGenVertexArrays(1, &vaoAxes);
        glBindVertexArray(vaoAxes);

        GLuint vboAxes;
        glGenBuffers(1, &vboAxes);
        glBindBuffer(GL_ARRAY_BUFFER,vboAxes);

        GLfloat vertexAxes[]= {
                0.0f, 0.0f, 0.0f,
                1.0f, 0.0f, 0.0f
        };

        GLuint eltBufAxes;
        glGenBuffers(1, &eltBufAxes);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eltBufAxes);

        glBufferData(GL_ARRAY_BUFFER, sizeof(vertexAxes), vertexAxes, GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), (void*)0);
        glEnableVertexAttribArray(0);

        GLint uni_HthieAxes = glGetUniformLocation(progAxes.getProgram(),"matHthie");
        GLint uni_TransAxes = glGetUniformLocation(progAxes.getProgram(),"vecTrans");
        GLint uni_MatriceAxes = glGetUniformLocation(progAxes.getProgram(),"matRot");
        GLint uni_ColorAxes = glGetUniformLocation(progAxes.getProgram(),"rgb");

        //variables de boucle d'évènement

        std::vector<glm::mat3> Hthie;
        std::vector<glm::vec3> Trans;
        int size;

        calcMatsTranslation(file, Trans, Hthie, size);

        bool fin = false;
        SDL_Event event;
        int width = 600;
        int height = 600;
        float x = 0.3;
        float lastx = 0.3;
        float y = 0.4;
        float lasty = 0.4;
        bool move=false;

        while(!fin)
        {
            while(SDL_PollEvent(&event))
            {
                switch(event.type)
                {
                    case SDL_QUIT:
                        fin = true;
                        break;
                    case SDL_MOUSEMOTION:
                        if(event.motion.x<lastx)
                        {
                            x=((move) ? x+0.05 : x);
                        }
                        if(event.motion.x>lastx)
                        {
                            x=((move) ? x-0.05 : x);
                        }
                        lastx=event.motion.x;
                        if(event.motion.y<lasty)
                        {
                            y=((move && y<1.5) ? y+0.05 : y);
                        }
                        if(event.motion.y>lasty)
                        {
                            y=((move && y>-1.5) ? y-0.05 : y);
                        }
                        lasty=event.motion.y;
                        break;
                    case SDL_MOUSEBUTTONDOWN:
                        move = true;
                        break;
                    case SDL_MOUSEBUTTONUP:
                        move = false;
                        break;
                    case SDL_KEYDOWN:
                        switch (event.key.keysym.sym)
                        {
                            case SDLK_LEFT:
                                x+=0.05;
                                break;
                            case SDLK_DOWN:
                                y = ((y<1.5) ? y+0.05 : y);
                                break;
                            case SDLK_RIGHT:
                                x-=0.05;
                                break;
                            case SDLK_UP:
                                y = ((y>-1.5) ? y-0.05 : y);
                                break;
                        }
                        break;
                    case SDL_WINDOWEVENT_SIZE_CHANGED:
                        width = event.window.data1;
                        height = event.window.data2;
                        break;
                    default :
                        break;
                }
            }
            glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

            //rafraichissement fond
            glUseProgram(progFond.getProgram());
            glBindVertexArray(vaoFond);

            glDisable(GL_DEPTH_TEST);

            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)0);
            glEnable(GL_DEPTH_TEST);

            //mise à jour des axes

            glUseProgram(progAxes.getProgram());
            glBindVertexArray(vaoAxes);

            for(int i=0;i<3;i++)
            {
                glUniformMatrix3fv(uni_HthieAxes, 1, GL_FALSE, glm::value_ptr(Hthie[size * size+i]));
                glUniform3fv(uni_TransAxes, 1, glm::value_ptr(Trans[size * size+i]));
                glUniformMatrix3fv(uni_MatriceAxes, 1, GL_FALSE, glm::value_ptr(matRot));
                glUniform1f(uni_ColorAxes, (float)i);

                glDrawArrays(GL_LINES, 0, 3);
            }

            //mise à jour du spectre

            glUseProgram(progSpectre.getProgram());
            glBindVertexArray(vaoSpectre);
            for (int i = 0;i<size*size;i++)
            {
                glUniformMatrix3fv(uni_Hthie, 1, GL_FALSE, glm::value_ptr(Hthie[i]));
                glUniform3fv(uni_Trans, 1, glm::value_ptr(Trans[i]));

                glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (void*)0);
            }

            SDL_GL_SwapWindow(win);

            //Rotation autour de l'axe y
            matRot = glm::mat3(cos(x)*((float)HEIGHT/WIDTH),0, -sin(x),
                                     0,1, 0,
                                     sin(x)*((float)HEIGHT/WIDTH), 0,cos(x));

            //Rotation autour de l'axe x
            matRotY = glm::mat3(1,0, 0,
                                0,cos(y), -sin(y),
                                0, sin(y),cos(y));

            matRot *= matRotY;//*glm::vec3(((float)HEIGHT/WIDTH),0,0);
            glUniformMatrix3fv(uni_Matrice, 1, GL_FALSE, glm::value_ptr(matRot));
        }
    }
    else
    {

    }

    SDL_Quit();
    file.close();

    return 0;
}

void calcMatsTranslation(std::fstream & file, std::vector<glm::vec3> & Trans, std::vector<glm::mat3> & Hthie, int & size)
{
    std::vector<int> vect(0);
    int val;
    char useless;

    while(!file.eof())
    {
        file >> val >> useless;
        vect.push_back(val);
    }

    size = sqrt(vect.size());
    int maxH = 0;

    //calcul du max d'unitée à faire rentrer dans [-1,1]
    for (int i = 0;i<size;i++)
    {
        for (int j =0;j<size;j++)
        {
            maxH = ((maxH>vect[i*size+j]) ? maxH : vect[i*size+j]);
        }
    }

    int max = ((size>maxH) ? size : maxH);

    //définir la transformation du contenu de la matrice vers l'espace [-1,1]

    GLfloat dimCube = (GLfloat)2.0/(max+1);//dimension du cube unitaire dans le cas présent
    GLfloat yMin = -dimCube*((GLfloat)maxH/2);//valeur minimale de Y
    GLfloat hCour;
    GLfloat largeP = (GLfloat)dimCube/0.5;

    int indice;

    for (int i = 0;i<size;i++)//z
    {
        for (int j =0;j<size;j++)//x
        {
            indice = i*size+j;
            hCour = (float)dimCube*(float)vect[indice];
            Hthie.push_back(glm::mat3(dimCube-0.05,0,0,
                                       0,hCour,0,
                                       0,0,dimCube-0.05));


            Trans.push_back(glm::vec3((j-size/2.0)*dimCube,yMin+hCour/2.0,(i-size/2.0)*dimCube));
        }
    }

    //axe Ox
    Hthie.push_back(glm::mat3(dimCube*(size+2), 0, 0,
                              0, 1, 0,
                              0, 0, 1));
    Trans.push_back(glm::vec3(-dimCube*(size+2)/2,yMin,-dimCube*(size+2)/2));

    //axe Oy
    Hthie.push_back(glm::mat3(0, 0, dimCube*(size+2),
                              0, 1, 0,
                              -1, 0, 0));
    Trans.push_back(glm::vec3(-dimCube*(size+2)/2,yMin,-dimCube*(size+2)/2));

    //axe Oz
    Hthie.push_back(glm::mat3(0, 1, 0,
                              -1, 0, 0,
                              0, 0, dimCube*(size+2)));
    Trans.push_back(glm::vec3(-dimCube*(size+2)/2,yMin,-dimCube*(size+2)/2));
}