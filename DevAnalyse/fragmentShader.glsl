//
//created by Justin GOUTEY on 08/02/2016.
//

#version 330 core

out vec4 color;
in vec3 color_out;

void main()
{
    float coloration = sqrt(color_out.x*color_out.x+color_out.y*color_out.y+color_out.z*color_out.z)/2;
    vec3 uv = (color_out+1)/2;
    color = vec4(uv.x, uv.z, 0.,1.);
}