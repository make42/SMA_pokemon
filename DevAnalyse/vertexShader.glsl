//
//created by Justin GOUTEY on 08/02/2016.
//

#version 330 core

layout(location = 0)in vec3 claude;
uniform mat3 matCToR;
uniform mat3 matRot;
uniform mat3 matProp;

uniform mat3 matHthie;
uniform vec3 vecTrans;

out vec3 color_out;

void main()
{
    color_out = claude;

    gl_Position = vec4(matRot*(matHthie*claude+vecTrans)*vec3(1.,1.,.25), 1.);
}