//
//created by Justin GOUTEY on 08/02/2016.
//

#version 330 core

layout(location = 0) in vec2 claude;

out vec2 claude_bis;

void main()
{
    claude_bis = claude;

    gl_Position = vec4(claude, 0.0, 1.0);
}
