//
// Created by stiven aigle on 14/12/15.
//

#ifndef OPENGL_PROGRAM_HPP
#define OPENGL_PROGRAM_HPP

#include <GL/glew.h>
#include <string>

class Program{
    private:
        GLuint programID;
    public:
        Program(char const * vertexShader,char const *  fragmentShader);
        Program();
        Program (Program const &) = delete;
        GLuint getProgram();
        void useProgram();
        ~Program();

};
#endif //OPENGL_PROGRAM_HPP
