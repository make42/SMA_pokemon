//
//created by Justin GOUTEY on 08/02/2016.
//

#version 330 core

layout (location = 0) in vec3 claude;

uniform mat3 matHthie;
uniform vec3 vecTrans;
uniform mat3 matRot;

out vec3 claude_bis;

void main()
{
    claude_bis = claude;

    gl_Position = vec4(matRot*(matHthie*claude+vecTrans)*vec3(1.,1.,.25),1.0);
}
